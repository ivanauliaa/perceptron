import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<List<Double>> dataTrain = readDataAndConvertToDouble("data_train.txt");
        List<Double> weights = getRandomWeightsAndBias(dataTrain.get(0).size(), 1, -1, 1); // last index adalah bias
        double learningRate = getRandomNumber(1, 0);
        int epochs = 1;

        // PRINT BEGIN
        System.out.println("Perceptron AND");
        System.out.println("=== === ===");
        System.out.println("Data Tranining: ");

        for (List<Double> row : dataTrain) {
            for (Double col : row) {
                System.out.print(col + "\t");
            }
            System.out.println();
        }

        System.out.println("Init weights: ");
        for (int i = 0; i < weights.size() - 1; i++) {
            System.out.print(String.format("%,.2f", weights.get(i)) + "\t");
        }
        System.out.println();

        System.out.println("Init bias: " + String.format("%,.2f", weights.get(weights.size() - 1)));
        System.out.println("Learning rate: " + String.format("%,.2f", learningRate));
        System.out.println("Epochs: " + epochs);
        System.out.println("=== === ===");
        // PRINT END

        weights = perceptron(dataTrain, weights, epochs, learningRate);

        // PRINT BEGIN
        System.out.print("FINAL WEIGHTS: ");
        for (int i = 0; i < weights.size() - 1; i++) {
            System.out.print(String.format("%,.2f", weights.get(i)) + "\t");
        }
        System.out.println();

        System.out.println("FINAL BIAS: " + String.format("%,.2f", weights.get(weights.size() - 1)));
        // PRINT END
    }

    /**
     * Baca data dari sebuah file lalu diconvert ke double
     * 
     * @param url file directory
     * @return data in list of list of double
     */
    public static List<List<Double>> readDataAndConvertToDouble(String url) {
        final String COMMA_DELIMITER = ",";
        List<List<Double>> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(url))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                Double[] doubleValues = new Double[values.length];
                for (int i = 0; i < values.length; i++) {
                    doubleValues[i] = Double.parseDouble(values[i]);
                }
                records.add(Arrays.asList(doubleValues));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }

    /**
     * Dapetin nilai random dengan range max dan min
     * 
     * @param max max range
     * @param min min range
     * @return random double number in range
     */
    public static double getRandomNumber(int max, int min) {
        return new Random().nextDouble() * (max - min) + min;
    }

    /**
     * Dapetin nilai weights secara random
     * 
     * @param length   sesuai banyak fitur
     * @param max      nilai max random
     * @param min      nilai min random
     * @param initBias nilai init bias
     * @return weights (weight per fitur)
     */
    public static List<Double> getRandomWeightsAndBias(int length, int max, int min, double initBias) {
        List<Double> weights = new ArrayList<>();

        for (int i = 0; i < length - 1; i++) {
            weights.add(getRandomNumber(max, min));
        }
        weights.add(initBias);

        return weights;
    }

    /**
     * Mencari weight terbaik dengan perceptron
     * 
     * @param dataTrain    list baris data training
     * @param weights      list weights
     * @param epochs       banyaknya epoch (siklus)
     * @param learningRate miu
     * @return weights terbaik
     */
    public static List<Double> perceptron(List<List<Double>> dataTrain, List<Double> weights, int epochs,
            double learningRate) {
        double globalError;
        int labelIndex = dataTrain.get(0).size() - 1;
        int i = 0;

        do {
            globalError = 0;

            System.out.println("EPOCH: " + (i + 1));
            System.out.println("--- --- ---");
            for (int j = 0; j < dataTrain.size(); j++) {
                double output = stepFunction(calculateOutput(weights, dataTrain.get(j)), 0);
                double localError = dataTrain.get(j).get(labelIndex) - output;

                // PRINT BEGIN
                System.out.println("ITERATION: " + (j + 1));
                System.out.println();
                System.out.print("FEATRS");
                for (int l = 0; l < dataTrain.get(j).size() - 1; l++) {
                    System.out.print("\t");
                }
                System.out.print("LABEL\t");
                System.out.print("WEIGHTS");
                for (int l = 0; l < weights.size() - 1; l++) {
                    System.out.print("\t");
                }
                System.out.print("BIAS\t");
                System.out.print("OUTPUT\t");
                System.out.println("LOC_ERR");

                for (int l = 0; l < dataTrain.get(j).size() - 1; l++) {
                    System.out.print(dataTrain.get(j).get(l) + "\t");
                }
                System.out.print(dataTrain.get(j).get(dataTrain.get(j).size() - 1) + "\t");
                for (int l = 0; l < weights.size() - 1; l++) {
                    System.out.print(String.format("%, .2f", weights.get(l)) + "\t");
                }
                System.out.print(String.format("%,.2f", weights.get(weights.size() - 1)) + "\t");
                System.out.print(String.format("%,.2f", output) + "\t");
                System.out.println(String.format("%,.2f", localError) + "\t");
                System.out.println();
                // PRINT END

                if (localError != 0) {
                    for (int k = 0; k < weights.size() - 1; k++) {
                        weights.set(k, weights.get(k) + learningRate * localError * dataTrain.get(j).get(k));
                    }

                    weights.set(weights.size() - 1, learningRate * localError);

                    // PRINT BEGIN
                    System.out.print("UPDATE WEIGHTS: ");
                    for (int k = 0; k < weights.size() - 1; k++) {
                        System.out.print(String.format("%,.2f", weights.get(k)) + "\t");
                    }
                    System.out.println();
                    System.out.println("UPDATE BIAS: " + String.format("%,.2f", weights.get(weights.size() - 1)));
                    // PRINT END
                }

                globalError += Math.abs(localError);
                System.out.println("GLOBAL ERROR: " + String.format("%,.2f", globalError));
                System.out.println("--- --- ---");
            }

            if (globalError == 0 && i < epochs) {
                System.out.println("Stopped before max epochs because of globalError = 0");
            }

            i++;
        } while (i < epochs && globalError != 0);

        return weights;
    }

    /**
     * Mencari prediksi output
     * 
     * @param weights      list dari weights
     * @param dataTrainRow list dari fitur
     * @return nilai prediksi output
     */
    public static double calculateOutput(List<Double> weights, List<Double> dataTrainRow) {
        double output = weights.get(weights.size() - 1);

        for (int i = 0; i < dataTrainRow.size() - 1; i++) {
            output += weights.get(i) * dataTrainRow.get(i);
        }

        return output;
    }

    /**
     * Fungsi aktivasi step
     * 
     * @param value
     * @return 1 jika value >= 0, 0 jika value < 0
     */
    public static double stepFunction(double value, double threshold) {
        return value >= threshold ? 1 : 0;
    }
}